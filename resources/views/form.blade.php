<!DOCTYPE html>
<html>

<head>
    <title>FormPendaftaran</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="{{route('welcome')}}" method="post">
        @csrf
        <!--Type Text -->
        <div>
            <label for="Fname">First Name</label>
            <br><br>
            <input type="text" id="Fname" name="FirstName">
            <br><br>
            <label for="Lname">Last Name</label>
            <br><br>
            <input type="text" id="Lname" name="LastName">
        </div>
        <br>
        <!--type radio-->
        <div>
            <p>Gender :</p>
            <br>
            <input type="radio" id="Male" name="Gender" value="Male">
            <label for="male">Male</label>
            <br>
            <input type="radio" id="Female" name="Gender" value="Female">
            <label for="Female">Female</label>
            <br>
            <input type="radio" id="Other" name="Gender" value="Other">
            <label for="Other">Other</label>
        </div>
        <br>
        <!--Select-->
        <div>
            <label>Nationality :</label>
            <br><br>
            <select>
                <option>Indonesian</option>
                <option>Singaporean</option>
                <option>Malaysyian</option>
                <option>Other</option>
            </select>
        </div>
        <br>
        <!--type Checkbox-->
        <div>
            <p>Languange spoken:</p>
            <br>
            <input type="Checkbox" id="Idn" name="Languange" value="Bahasa Indonesia">
            <label for="Idn">Bahasa indonesia</label>
            <br>
            <input type="Checkbox" id="Eng" name="Languange" value="English">
            <label for="Eng">English</label>
            <br>
            <input type="Checkbox" id="Oth" name="Languange" value="Other">
            <label for="Oth">Other</label>
        </div>
        <br>
        <!--Text area-->
        <div>
            <p>Bio :</p>
            <textarea cols="30" rows="10"></textarea>
        </div>
        <br>
        <!--Button-->
        <div>
            <input type="submit" value="Sign Up">
        </div>

    </form>

</body>

</html>